# phylum-analyze-pr-gitlab-test

## Usage
1. Copy `.gitlab-ci.yml` to your repository
2. Manually create a Phylum project for your repository
`phylum projects create <name>`
3. Add, commit and push `.phylum_project` file to repository
`git add .phylum_project; git commit -m "added .phylum_project"; git push`
4. Create a new `file` GitLab CI Variable and paste the contents of your local `$HOME/.phylum/settings.yaml`
5. 
